<?php

namespace tests;

use app\models;
use app\models\GitlabRepo;

/**
 * GitlabRepoTest contains test casess for gitlab repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class GitlabRepoTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
       $this->gitlab = new GitlabRepo('Artur', 12, 2);
    }

    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCount()
    {
        $this->assertEquals(12, $this->gitlab->getForkCount());
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testData()
    {
        $excepted = ['name' => 'Artur', 
                     'fork-count' => 12,
                     'rating' => 12.5,
                     'start-count' => 2];                    
        $this->assertEquals($excepted, $this->gitlab->getData());
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringify()
    {
       
        $excepted = 'Artur                                                                         12 ⇅    2 ★';          
        $this->assertEquals($excepted, $this->gitlab->__toString());

    }
}