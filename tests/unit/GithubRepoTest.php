<?php

namespace tests;

use app\models;
use app\models\GithubRepo;

/**
 * GithubRepoTest contains test casess for github repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class GithubRepoTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
       $this->github = new GithubRepo('Artur', 12, 2, 4);
    }

    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCount()
    {
        $this->assertEquals(12, $this->github->getForkCount());
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testData()
    {
         $excepted = ['name' => 'Artur', 
                     'fork-count' => 12,
                     'watcher-count' => 4,
                     'rating' => 9.666666666666666,
                     'start-count' => 2];                    
        $this->assertEquals($excepted, $this->github->getData());
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringify()
    {
        $excepted = 'Artur                                                                         12 ⇅    2 ★    4 👁️';          
        $this->assertEquals($excepted, $this->github->__toString());
    }
}