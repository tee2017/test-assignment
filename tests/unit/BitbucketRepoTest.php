<?php

namespace tests;

use app\models;
use app\models\BitbucketRepo;

/**
 * BitbucketRepoTest contains test casess for bitbucket repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BitbucketRepoTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
       $this->bitbucket = new BitbucketRepo('Artur', 12, 2);
    }

    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCount()
    {
       $this->assertEquals(12, $this->bitbucket->getForkCount());
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testData()
    {
        
        $excepted = ['name' => 'Artur', 
                     'fork-count' => 12,
                     'watcher-count' => 2,
                     'rating' => 13.0];                    
        $this->assertEquals($excepted, $this->bitbucket->getData());

    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringify()
    {
       
        $excepted = 'Artur                                                                         12 ⇅           2 👁️';          
        $this->assertEquals($excepted, $this->bitbucket->__toString());

    }
}