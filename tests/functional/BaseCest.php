<?php

/**
 * Base contains test cases for tesing api endpoint 
 * 
 * @see https://codeception.com/docs/modules/Yii2
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BaseCest
{
    /**
     * Example test case
     *
     * @return void
     */
//     public function cestExamle(\FunctionalTester $I)
//     {
//         $I->amOnPage([
//             'base/api',
//             'users' => [
//                 'kfr',
//             ],
//             'platforms' => [
//                 'github',
//             ]
//         ]);
//         $expected = json_decode('[
//             {
//                 "name": "kfr",
//                 "platform": "github",
//                 "total-rating": 1.5,
//                 "repos": [],
//                 "repo": [
//                     {
//                         "name": "kf-cli",
//                         "fork-count": 0,
//                         "start-count": 2,
//                         "watcher-count": 2,
//                         "rating": 1
//                     },
//                     {
//                         "name": "cards",
//                         "fork-count": 0,
//                         "start-count": 0,
//                         "watcher-count": 0,
//                         "rating": 0
//                     },
//                     {
//                         "name": "UdaciCards",
//                         "fork-count": 0,
//                         "start-count": 0,
//                         "watcher-count": 0,
//                         "rating": 0
//                     },
//                     {
//                         "name": "unikgen",
//                         "fork-count": 0,
//                         "start-count": 1,
//                         "watcher-count": 1,
//                         "rating": 0.5
//                     }
//                 ]
//             }
//         ]');
//         $I->assertEquals($expected, json_decode($I->grabPageSource()));
//     }
//     /**
//      * Test case for api with bad request params
//      *
//      * @return void
//      */
//     public function cestBadParams(\FunctionalTester $I)
//     {

//         $I->amOnPage([
//             'base/api',
//             'users' => [
//                 3424,
//             ],
//             'platforms' => [
//                 'github',
//             ]
//         ]);
       
//         $I->assertEmpty(json_decode($I->grabPageSource()));
//     }

//     /**
//      * Test case for api with empty user list
//      *
//      * @return void
//      */
//     public function cestEmptyUsers(\FunctionalTester $I)
//     {
//         $I->amOnPage([
//             'base/api',
//             'users' => [],
//             'platforms' => ['github',]
//         ]);
//         $response = strip_tags($I->grabPageSource());
//         $I->assertEquals('Bad Request: Missing required parameters: users', $response);
//     }

//     /**
//      * Test case for api with empty platform list
//      *
//      * @return void
//      */
//     public function cestEmptyPlatforms(\FunctionalTester $I)
//     {
//         $I->amOnPage([
//             'base/api',
//             'users' => ['kfr'],
//             'platforms' => []
//         ]);
//         $response = strip_tags($I->grabPageSource());
//         $I->assertEquals('Bad Request: Missing required parameters: platforms', $response);
//     }

//     /**
//      * Test case for api with non empty platform list
//      *
//      * @return void
//      */
//     public function cestSeveralPlatforms(\FunctionalTester $I)
//     {
//         $I->amOnPage([
//             'base/api',
//             'users' => [
//                 'kfr',
//             ],
//             'platforms' => [
//                 'github',
//             ]
//         ]);

//         $response = json_decode($I->grabPageSource(), true);
//         $I->assertNotEmpty($response[0]['platform']);
//     }

//     /**
//      * Test case for api with non empty user list
//      *
//      * @return void
//      */
//     public function cestSeveralUsers(\FunctionalTester $I)
//     {
//         $I->amOnPage([
//             'base/api',
//             'users' => [
//                 'kfr',
//             ],
//             'platforms' => [
//                 'github',
//             ]
//         ]);

//         $response = json_decode($I->grabPageSource(), true);
//         $I->assertNotEmpty($response[0]['platform']);
//     }

//     /**
//      * Test case for api with unknown platform in list
//      *
//      * @return void
//      */
//     public function cestUnknownPlatforms(\FunctionalTester $I)
//     {
//         $I->amOnPage([
//             'base/api',
//             'users' => [
//                 'kfr',
//             ],
//             'platforms' => [
//                 'source',
//             ]
//         ]);
       
//         $I->assertEmpty(json_decode($I->grabPageSource()));
//     }

//     /**
//      * Test case for api with unknown user in list
//      *
//      * @return void
//      */
//     public function cestUnknowUsers(\FunctionalTester $I)
//     {
//         $I->amOnPage([
//             'base/api',
//             'users' => [
//                 'userUnKnOwN',
//             ],
//             'platforms' => [
//                 'github',
//             ]
//         ]);
       
//         $I->assertEmpty(json_decode($I->grabPageSource()));
//     }

//     /**
//      * Test case for api with mixed (unknown, real) users and non empty platform list
//      *
//      * @return void
//      */
//    public function cestMixedUsers(\FunctionalTester $I)
//    {
//        $I->amOnPage([
//            'base/api',
//            'users' => [
//                'userUnKnOwN',
//                'kfr'
//            ],
//            'platforms' => [
//                'github',
//            ]
//        ]);
//        $expected = json_decode('[
//            {
//                "name": "kfr",
//                "platform": "github",
//                "total-rating": 1.5,
//                "repos": [],
//                "repo": [
//                    {
//                        "name": "kf-cli",
//                        "fork-count": 0,
//                        "start-count": 2,
//                        "watcher-count": 2,
//                        "rating": 1
//                    },
//                    {
//                        "name": "cards",
//                        "fork-count": 0,
//                        "start-count": 0,
//                        "watcher-count": 0,
//                        "rating": 0
//                    },
//                    {
//                        "name": "UdaciCards",
//                        "fork-count": 0,
//                        "start-count": 0,
//                        "watcher-count": 0,
//                        "rating": 0
//                    },
//                    {
//                        "name": "unikgen",
//                        "fork-count": 0,
//                        "start-count": 1,
//                        "watcher-count": 1,
//                        "rating": 0.5
//                    }
//                ]
//            }
//        ]');
//        $I->assertEquals($expected, json_decode($I->grabPageSource()));
//    }

//     /**
//      * Test case for api with mixed (github, gitlab, bitbucket) platforms and non empty user list
//      *
//      * @return void
//      */
//      public function cestMixedPlatforms(\FunctionalTester $I)
//      {
//          $I->amOnPage([
//              'base/api',
//              'users' => [
//                  'kfr',
//              ],
//              'platforms' => [
//                  'github',
//                  'gitlab',
//                  'bitbucket',
//              ]
//          ]);
//          $expected = json_decode('[
//             {
//                 "name": "kfr",
//                 "platform": "github",
//                 "total-rating": 1.5,
//                 "repos": [],
//                 "repo": [
//                     {
//                         "name": "kf-cli",
//                         "fork-count": 0,
//                         "start-count": 2,
//                         "watcher-count": 2,
//                         "rating": 1
//                     },
//                     {
//                         "name": "cards",
//                         "fork-count": 0,
//                         "start-count": 0,
//                         "watcher-count": 0,
//                         "rating": 0
//                     },
//                     {
//                         "name": "UdaciCards",
//                         "fork-count": 0,
//                         "start-count": 0,
//                         "watcher-count": 0,
//                         "rating": 0
//                     },
//                     {
//                         "name": "unikgen",
//                         "fork-count": 0,
//                         "start-count": 1,
//                         "watcher-count": 1,
//                         "rating": 0.5
//                     }
//                 ]
//             }
//         ]');
//         $I->assertEquals($expected, json_decode($I->grabPageSource()));
//      }
}